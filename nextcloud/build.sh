#!/usr/bin/env bash

VERSION=$1
NEXTCLOUD_RELEASE=$2

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [version] [nextcloud_release]"
    echo ""
    echo "Args:"
    echo "	version: The version of the current container (e.g. ls4)"
    echo "	nextcloud_release: The release of nextcloud to include (e.g. 21.0.1 or php8-21.0.1)"
}

function build_image() {
    IMAGE=$1
    ARCH=$2

    sed "s/BASE_TAG/${ARCH}-${NEXTCLOUD_RELEASE}-${VERSION}/g" "Dockerfile.${ARCH}" \
        > "Dockerfile.${ARCH}.${NEXTCLOUD_RELEASE}.${VERSION}"

    sudo docker build \
        --pull \
        --no-cache \
        -f "Dockerfile.${ARCH}.${NEXTCLOUD_RELEASE}.${VERSION}" \
        -t "${IMAGE}:${NEXTCLOUD_RELEASE}-${VERSION}-${ARCH}" \
        -t "${IMAGE}:latest-${ARCH}" \
        -t "${IMAGE}:latest" \
        .

    sudo docker push "${IMAGE}:${NEXTCLOUD_RELEASE}-${VERSION}-${ARCH}"
    sudo docker push "${IMAGE}:latest-${ARCH}"
    sudo docker push "${IMAGE}:latest"

    rm "Dockerfile.${ARCH}.${NEXTCLOUD_RELEASE}.${VERSION}"
}

require "$VERSION" "version"
require "$NEXTCLOUD_RELEASE" "nextcloud release"

if ! sudo docker run --rm -it arm64v8/ubuntu:19.10 /bin/bash -c 'echo "docker is configured correctly"'; then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

set -xe

build_image asonix/nextcloud arm64v8
