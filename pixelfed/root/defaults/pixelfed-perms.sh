#!/bin/bash

pfpath="${PIXELFED_PATH}"
htuser='abc'
htgroup='abc'
rootuser='root'

printf "chmod Files and Directories\n"
find ${pfpath}/ -type f -print0 | xargs -0 chmod 0640
find ${pfpath}/ -type d -print0 | xargs -0 chmod 0750

printf "chown Directories\n"
chown -R ${rootuser}:${htgroup} ${pfpath}/
chown -R ${htuser}:${htgroup} ${pfpath}/app/
chown -R ${htuser}:${htgroup} ${pfpath}/bootstrap/
chown -R ${htuser}:${htgroup} ${pfpath}/config/
chown -R ${htuser}:${htgroup} ${pfpath}/public/
chown -R ${htuser}:${htgroup} ${pfpath}/resources/
chown -R ${htuser}:${htgroup} ${pfpath}/routes/
chown -R ${htuser}:${htgroup} ${pfpath}/storage/

chmod +x ${pfpath}/artisan

printf "chmod/chown .htaccess\n"
if [ -f ${pfpath}/public/.htaccess ]
 then
  chmod 0644 ${pfpath}/public/.htaccess
  chown ${rootuser}:${htgroup} ${pfpath}/.htaccess
fi
