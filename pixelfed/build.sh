#!/usr/bin/env bash

BUILD_DATE=$(date)
VERSION=$1
PIXELFED_RELEASE=$2

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [version] [pixelfed_release]"
    echo ""
    echo "Args:"
    echo "	version: The version of the current container"
    echo "	pixelfed_release: The release of pixelfed to include"
}

function build_image() {
    IMAGE=$1
    ARCH=$2

    docker build \
        --pull \
        --no-cache \
        --build-arg BUILD_DATE="${BUILD_DATE}" \
        --build-arg PIXELFED_RELEASE="${PIXELFED_RELEASE}" \
        --build-arg VERSION="${VERSION}" \
        -f "Dockerfile.${ARCH}" \
        -t "${IMAGE}:${PIXELFED_RELEASE}-${VERSION}-${ARCH}" \
        -t "${IMAGE}:latest-${ARCH}" \
        -t "${IMAGE}:latest" \
        .

    docker push "${IMAGE}:${PIXELFED_RELEASE}-${VERSION}-${ARCH}"
    docker push "${IMAGE}:latest-${ARCH}"
    docker push "${IMAGE}:latest"
}

require "$VERSION" "version"
require "$PIXELFED_RELEASE" "pixelfed release"

set -xe

build_image asonix/pixelfed arm64v8
