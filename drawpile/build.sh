#!/usr/bin//env bash

VERSION=$1

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "container.sh"
    echo ""
    echo "Usage:"
    echo "	container.sh [version]"
    echo ""
    echo "Args:"
    echo "	version: The version of dpserver to build"
}

function build_image() {
    IMAGE=$1
    ARCH=$2

    docker build \
        --pull \
        --no-cache \
        --build-arg "version=${VERSION}" \
        -f "Dockerfile.${ARCH}" \
        -t "${IMAGE}:${VERSION}-${ARCH}" \
        -t "${IMAGE}:latest-${ARCH}" \
        -t "${IMAGE}:latest" \
        .

    docker push "${IMAGE}:${VERSION}-${ARCH}"
    docker push "${IMAGE}:latest-${ARCH}"
    docker push "${IMAGE}:latest"
}

require "$VERSION" "version"

set -xe

# pushd dpserver
# build_image asonix/drawpile-server arm64v8
# popd
pushd listserver
build_image asonix/drawpile-listserver arm64v8
popd
pushd static
build_image asonix/drawpile-static arm64v8
