#!/usr/bin/env sh

set -xe

function require() {
    if [ "$1" = "" ]; then
        echo "required variable $2 missing, bailing"
        exit 1
    fi
}

function optional() {
    if [ "$1" = "" ]; then
        echo "optional variable $2 missing, skipping"
    fi
}

function replace() {
    sed "s/;*$1=.*/$1=$2/g" /etc/murmur.ini > /tmp/murmur.ini
    cat /tmp/murmur.ini > /etc/murmur.ini
    rm /tmp/murmur.ini
}

function optional_replace() {
    if [ "$2" != "" ]; then
        replace "$1" "$2"
    fi
}

optional "$WELCOME_TEXT" "WELCOME_TEXT"
optional "$REGISTER_NAME" "REGISTER_NAME"
optional "$REGISTER_URL" "REGISTER_URL"
optional "$HOST" "HOST"
optional "$SERVER_PASSWORD" "SERVER_PASSWORD"
optional "$REGISTER_PASSWORD" "REGISTER_PASSWORD"
optional "$REGISTER_HOSTNAME" "REGISTER_HOSTNAME"
optional "$CERT_REQUIRED" "CERT_REQUIRED"
optional "$SSL_CERT_PATH" "SSL_CERT_PATH"
optional "$SSL_KEY_PATH" "SSL_KEY_PATH"

optional_replace "welcometext" "$WELCOME_TEXT"
optional_replace "registerName" "$REGISTER_NAME"
optional_replace "registerUrl" "$REGISTER_URL"
optional_replace "host" "$HOST"
optional_replace "serverpassword" "$SERVER_PASSWORD"
optional_replace "registerPassword" "$REGISTER_PASSWORD"
optional_replace "registerHostname" "$REGISTER_HOSTNAME"
optional_replace "certrequired" "$CERT_REQUIRED"
optional_replace "sslCert" "$SSL_CERT_PATH"
optional_replace "sslKey" "$SSL_KEY_PATH"

exec "$@"
