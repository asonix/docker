#!/usr/bin//env bash

TAG=$1

function require() {
	if [ "$1" = "" ]; then
		echo "input '$2' required"
		print_help
		exit 1
	fi
}

function print_help() {
	echo "build.sh"
	echo ""
	echo "Usage:"
	echo "	build.sh [tag]"
	echo ""
	echo "Args:"
	echo "	tag: The tag of the mautrix-telegram version that is being packaged"
}

require "$TAG" tag

set -xe

docker build \
	--pull \
	--no-cache \
        --build-arg TAG="${TAG}" \
	-f Dockerfile \
	-t "asonix/mautrix-telegram:$TAG-arm64v8" \
	-t asonix/mautrix-telegram:latest-arm64v8 \
	-t asonix/mautrix-telegram:latest \
	.

docker push "asonix/mautrix-telegram:$TAG-arm64v8"
docker push asonix/mautrix-telegram:latest-arm64v8
docker push asonix/mautrix-telegram:latest
