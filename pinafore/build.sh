#!/usr/bin//env bash

DEFAULT_REPOSITORY=https://github.com/nolanlawson/pinafore

BUILD_DATE=$(date)
VERSION=$1
TAG=$2
GIT_REPOSITORY=${3:-$DEFAULT_REPOSITORY}

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [version] [tag] [repository]"
    echo ""
    echo "Args:"
    echo "	version: The version of the current container, ex. r0"
    echo "	tag: The tag of pinafore to include, ex 1.15.8"
    echo "	repository: The git repository to fetch pinafore from (optional, defaults to https://github.com/nolanlawson/pinafore)"
}

function build_image() {
    IMAGE=$1
    ARCH=$2

    docker build \
        --pull \
        --no-cache \
        --build-arg BUILD_DATE="${BUILD_DATE}" \
        --build-arg TAG="${TAG}" \
        --build-arg VERSION="${VERSION}" \
        --build-arg GIT_REPOSITORY="${GIT_REPOSITORY}" \
        -f "Dockerfile.${ARCH}" \
        -t "${IMAGE}:$(echo ${TAG} | sed 's/\//-/g')-${VERSION}-${ARCH}" \
        -t "${IMAGE}:latest-${ARCH}" \
        -t "${IMAGE}:latest" \
        .

    docker push "${IMAGE}:$(echo ${TAG} | sed 's/\//-/g')-${VERSION}-${ARCH}"
    docker push "${IMAGE}:latest-${ARCH}"
    docker push "${IMAGE}:latest"
}

require "$VERSION" "version"
require "$TAG" "tag"

if ! docker run --rm -it arm64v8/ubuntu:19.10 /bin/bash -c 'echo "docker is configured correctly"'; then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

set -xe

build_image asonix/pinafore arm64v8
