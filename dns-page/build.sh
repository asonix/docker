#!/usr/bin//env bash

TAG=$1

function require() {
	if [ "$1" = "" ]; then
		echo "input '$2' required"
		print_help
		exit 1
	fi
}

function print_help() {
	echo "build.sh"
	echo ""
	echo "Usage:"
	echo "	build.sh [tag]"
	echo ""
	echo "Args:"
	echo "	tag: The tag of the generated container"
}

require "$TAG" tag

if ! docker run --rm -it arm64v8/ubuntu:19.10 /bin/bash -c 'echo "docker is configured correctly"'; then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

set -xe

docker build \
	--pull \
	--no-cache \
	-f "Dockerfile" \
	-t "asonix/dns-page:$TAG-arm64v8" \
	-t asonix/dns-page:latest-arm64v8 \
	-t asonix/dns-page:latest \
	.

docker push "asonix/dns-page:$TAG-arm64v8"
docker push asonix/dns-page:latest-arm64v8
docker push asonix/dns-page:latest
