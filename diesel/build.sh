#!/usr/bin//env bash

BUILD_DATE=$(date)
VERSION=$1

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [version]"
    echo ""
    echo "Args:"
    echo "	version: The version of the current container"
}

function build_image() {
    IMAGE=$1
    ARCH=$2

    docker build \
        --pull \
        --no-cache \
        --build-arg BUILD_DATE="${BUILD_DATE}" \
        --build-arg VERSION="${VERSION}" \
        -f "Dockerfile.${ARCH}" \
        -t "${IMAGE}:${VERSION}-${ARCH}" \
        -t "${IMAGE}:latest-${ARCH}" \
        -t "${IMAGE}:latest" \
        .

    docker push "${IMAGE}:${VERSION}-${ARCH}"
    docker push "${IMAGE}:latest-${ARCH}"
    docker push "${IMAGE}:latest"
}

require "$VERSION" "version"

set -xe

build_image asonix/diesel-cli arm64v8
