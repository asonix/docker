#!/usr/bin/env bash

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "deploy.sh"
    echo ""
    echo "Usage:"
    echo "	deploy.sh [tag] [push]"
    echo ""
    echo "Args:"
    echo "	tag: The git tag to be applied to the repository and docker build"
    echo "	push: Whether or not to push the image"
    echo ""
    echo "Examples:"
    echo "	./deploy.sh v0.2.0-r0 main true"
    echo "	./deploy.sh v0.2.0-r0 asonix/shell-out"
}

REPO=dimension

function build_image() {
    tag=$1
    arch=$2
    push=$3

    sudo docker build \
        --pull \
        --build-arg TAG=$tag \
        --build-arg ARCH=$arch \
        -t asonix/$REPO:$arch-$tag \
        -f Dockerfile \
        .

    sudo docker tag \
        asonix/$REPO:$arch-$tag \
        asonix/$REPO:$arch-latest

    if [ "$push" == "true" ]; then
        sudo docker push asonix/$REPO:$arch-$tag
        sudo docker push asonix/$REPO:$arch-latest
    fi
}

function manifest() {
  tag=$1
  push=$2

  sudo docker manifest create asonix/$REPO:$tag \
      -a asonix/$REPO:arm64v8-$tag \
      -a asonix/$REPO:amd64-$tag

  sudo docker manifest annotate asonix/$REPO:$tag \
      asonix/$REPO:arm64v8-$tag --os linux --arch arm64 --variant v8

  # sudo docker manifest annotate asonix/$REPO:$tag \
  #     asonix/$REPO:arm32v7-$tag --os linux --arch arm --variant v7

  sudo docker manifest annotate asonix/$REPO:$tag \
      asonix/$REPO:amd64-$tag --os linux --arch amd64

  if [ "$push" == "true" ]; then
      sudo docker manifest push asonix/$REPO:$tag --purge
  fi
}

# Creating the new tag
tag="$1"
push=${2:-false}

require "$tag" "tag"
require "$push" "push"

if ! sudo docker run --rm -it arm64v8/alpine:3.14 /bin/sh -c 'echo "docker is configured correctly"'
then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

set -xe

# Build for arm64v8, arm32v7 and amd64
build_image $tag arm64v8 $push
# build_image $tag arm32v7 $push
build_image $tag amd64 $push

# Build for other archs
# TODO

if [ "$push" == "true" ]; then
    manifest $tag $push
    manifest latest $push
fi
